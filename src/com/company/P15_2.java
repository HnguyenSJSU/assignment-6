package com.company;

/**
 * Hung Nguyen
 * 013626210
 * Asks user to interact with UI to add,remove,and modify existing student
 */

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class P15_2 {
    //Scanner to read input
    public static final Scanner in = new Scanner(System.in);
    //Linked hashmap to store input order
    public static LinkedHashMap<Student,String> studentOrderedGrades= new LinkedHashMap<>();
    //store list of existing IDs
    public static ArrayList<String> IDList = new ArrayList<>();
    /**
     * return a valid name
     * @param nType
     * @return strCap
     */
    public static String getUserName(int nType){

        if(nType == 1) {
            System.out.print("Enter student first name: ");
        }
        else if (nType == 2){
            System.out.print("Enter student last name: ");
        }
        String str = in.nextLine();
        //check for special characters
        Pattern p = Pattern.compile("[^A-Za-z0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        String strCap = "";
        //if there exists special characters or numbers
        while(str.matches(".*\\d.*") || m.find() == true){
            //asks user to reenter input if special characters are detected
            if(m.find()) {
                System.out.println(String.format("Input %s contains special characters, please reenter letters only:", str));
                str = in.nextLine();
                m = p.matcher(str);
            }
            //asks user to reenter input if numbers are detected
            else if(str.matches(".*\\d.*")) {
                //replace non numbers with none
                String s = str.replaceAll("[^0-9]", "");
                System.out.print(String.format("Number %s is not allowed, please reenter letters only: ", s));
                str = in.nextLine();
                m = p.matcher(str);
            }
        };
        //return uppercase name
        strCap = str.substring(0, 1).toUpperCase() + str.substring(1);
        return strCap;
    }

    /**
     * get a valid id that only contains numbers and letters
     * @param nType
     * @return strId
     */
    public static String getStudentId(int nType){
        System.out.print("Please enter student id: ");
        String strId = in.nextLine();
        //check if student id contains special characters
        Pattern p = Pattern.compile("[^A-Za-z0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(strId);
        //if found
        while(m.find() == true){
            System.out.print(String.format("Input %s contains special characters, please reenter letters or numbers only: ",strId));
            strId = in.nextLine();
            m = p.matcher(strId);
        }
        //Determine if we need to check IDlist to see if there is already an existing ID
        if (nType == 1) {
            while (IDList.contains(strId) == true) {
                System.out.print(String.format("Id is not unique, please reenter ", strId));
                strId = in.nextLine();
            }
            IDList.add(strId);
        }

        return strId;
    }

    /**
     * get a valid grade input
     * @return strGrade
     */
    public static String getStudentGrade(){
        System.out.print("Please enter student Grade: ");
        //store user input
        String strGrade = in.nextLine();
        //store first letter of input
        char cGrade = 1;
        //if wrong format
        while(strGrade.charAt(0) == '-'){
            System.out.print("Please enter student Grade in format (A-): ");
            strGrade = in.nextLine();
        }
        //uppercase first letter of userinput
        cGrade = strGrade.toUpperCase().charAt(0);
        //if correct format but wrong letters
        while(cGrade != 'A' && cGrade != 'B'  && cGrade != 'C' && cGrade != 'D'
                && cGrade != 'F') {
            System.out.println(String.format("%s is not a valid grade",strGrade));
            System.out.print("Please reenter student Grade: ");
            strGrade = in.nextLine();
            cGrade = strGrade.toUpperCase().charAt(0);
        }
        //if Grade doesn't have extra symbol
        if (strGrade.length() > 1) {
            strGrade = cGrade + Character.toString(strGrade.charAt(1));
        }else{
            strGrade = Character.toString(cGrade);
        }
        return strGrade;
    }

    /**
     * find if id exists in hashmap
     * @param strTemp
     * @return
     */
    public static Student findID(String strTemp){
        Student student = null;
        for(Map.Entry<Student,String> temp: studentOrderedGrades.entrySet() ){
            if(temp.getKey().getStrID().equals(strTemp)){
                student = temp.getKey();
            }
        }

        return student;
    }

    /**
     * Asks user to interact with menu
     * @param args
     */
    public static void main(String[] args) {

        //hashmap to store ID and grades
        HashMap<String,String> studentID = new HashMap<String,String>();
        //List to store objects of student
        ArrayList<Student> orderedStudent = new ArrayList<Student>();
        //store user choice
        char cUserInput = 1;
        //if userchoice isn't q
        while(cUserInput != 'q') {
            System.out.print("""
                  Select an option: 
                  (a)to add a student
                  (r)to remove a student 
                  (m)to modify a grade
                  (p)print all grades
                  (q)to quit
                  Enter choice:  """);
            String strTemp = in.nextLine();
            //store user choice
            cUserInput = strTemp.charAt(0);
            //store user's name and grade
            String strFirst = "";
            String strGrade = "";
            String strLast = "";
            String strID = "";
            Student tempStudent = null;
            switch(cUserInput){
                case 'a':
                    //asks user to enter info
                    strFirst = getUserName(1);
                    strLast = getUserName(2);
                    strGrade = getStudentGrade();
                    strID = getStudentId(1);
                    //if both aren't blank
                    if(!strFirst.equals("") && !strGrade.equals("")
                            && !strGrade.equals("") && !strID.equals("")){
                        studentOrderedGrades.put(new Student(strFirst,strLast,strID),strGrade);
                        orderedStudent.add(new Student(strFirst,strLast,strID));
                        studentID.put(strID,strGrade);
                        System.out.println("Added student successfully");
                    }
                    break;
                case 'r':
                    //get id
                    strID = getStudentId(2);
                    tempStudent = findID(strID);
                    //check if student in hashmap
                    if(tempStudent != null){
                        System.out.println(String.format("Student %s removed successfully",strID));
                        studentOrderedGrades.remove(tempStudent);
                        orderedStudent.remove(orderedStudent.indexOf(tempStudent));
                        IDList.remove(IDList.indexOf(strID));
                        studentID.remove(tempStudent);
                    }// if student isn't in hashmap
                    else {
                        System.out.println("Student doesn't exist");
                    }
                    break;
                case 'm':
                    strID = getStudentId(2);
                    strGrade = getStudentGrade();
                    //if student exists
                    tempStudent = findID(strID);

                    //if(studentGrades.containsKey(tempStudent)){
                    if(tempStudent != null){
                        //update new grade
                        studentOrderedGrades.put(tempStudent,strGrade);
                        studentID.put(strID,strGrade);


                        System.out.println("Student grade modified successfully");
                    }
                    else {//if student doesn't exist
                        System.out.println(String.format("Student %s doesn't exist", strID ));
                    }
                    break;
                case 'p':
                    //if there are existing student in list
                    if(!orderedStudent.isEmpty()) {
                        //clear out the linked hashmap
                        studentOrderedGrades.clear();
                        //sort the orderedStudent list using compareTo method
                        Collections.sort(orderedStudent,Student::compareTo);
                        //put list into Linked hashmap
                        for (int i = 0; i < orderedStudent.size(); i++){
                            studentOrderedGrades.put(orderedStudent.get(i), studentID.get(orderedStudent.get(i).getStrID()));
                        }
                        //print out linked hashmap
                        for (Map.Entry<Student, String> temp : studentOrderedGrades.entrySet()) {
                            //storing keys and values
                            System.out.println(String.format("%s %s",temp.getKey(),temp.getValue()));
                        }



                    }
                    else {//if no students in hashmap
                        System.out.println("No students");
                    }
                    break;
                case 'q':
                    break;
                default:
                    System.out.println(String.format("%s is not a correct input",strTemp));
                    System.out.println();
            }
        }

    }
}

