package com.company;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Asks user to interact with menu
 */
public class E15_4 {
    public static final Scanner in = new Scanner(System.in);

    /**
     * return a valid name
     * @param nType
     * @return strCap
     */
    public static String getName(int nType){

        if(nType == 1) {
            System.out.print("Enter student first name: ");
        }
        else if (nType == 2){
            System.out.print("Enter student last name: ");
        }
        String str = in.nextLine();
        //check for special characters
        Pattern p = Pattern.compile("[^A-Za-z0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        String strCap = "";
        //if there exists special characters or numbers
        while(str.matches(".*\\d.*") || m.find() == true){
            //asks user to reenter input if special characters are detected
            if(m.find()) {
                System.out.println(String.format("Input %s contains special characters, please reenter letters only:", str));
                str = in.nextLine();
                m = p.matcher(str);
            }
            //asks user to reenter input if numbers are detected
            else if(str.matches(".*\\d.*")) {
                //replace non numbers with none
                String s = str.replaceAll("[^0-9]", "");
                System.out.print(String.format("Number %s is not allowed, please reenter letters only: ", s));
                str = in.nextLine();
                m = p.matcher(str);
            }
            else{
                System.out.println("Valid input");
            }
        };
        //return uppercase name
        strCap = str.substring(0, 1).toUpperCase() + str.substring(1);
        return strCap;
    }

    /**
     * get a valid id that only contains numbers and letters
     * @return strId
     */
    public static String getId(){
        System.out.print("Please enter student id: ");
        String strId = in.nextLine();
        //check if student id contains special characters
        Pattern p = Pattern.compile("[^A-Za-z0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(strId);
        //if found
        while(m.find() == true){
            System.out.print(String.format("Input %s contains special characters, please reenter letters or numbers only: ",strId));
            strId = in.nextLine();
            m = p.matcher(strId);
        }
        return strId;
    }

    /**
     * get a valid grade input
     * @return strGrade
     */
    public static String getGrade(){
        System.out.print("Please enter student Grade: ");
        //store user input
        String strGrade = in.nextLine();
        //store first letter of input
        char cGrade = 1;
        //if wrong format
        while(strGrade.charAt(0) == '-'){
            System.out.print("Please enter student Grade in format (A-): ");
            strGrade = in.nextLine();
        }
        //uppercase first letter of userinput
        cGrade = strGrade.toUpperCase().charAt(0);
        //if correct format but wrong letters
        while(cGrade != 'A' && cGrade != 'B'  && cGrade != 'C' && cGrade != 'D'
                && cGrade != 'F') {
            System.out.println(String.format("%s is not a valid grade",strGrade));
            System.out.print("Please reenter student Grade: ");
            strGrade = in.nextLine();
            cGrade = strGrade.toUpperCase().charAt(0);
        }
        return strGrade;
    }

    /**
     * Asks user to interact with menu
     * @param args
     */
    public static void main(String[] args) {
        //hashmap to store name and grades
        HashMap<String,String> studentGrades = new HashMap<String,String>();
        //store user choice
        char cUserInput = 1;
        //if userchoice isn't q
        while(cUserInput != 'q') {
            System.out.print("""
                  Select an option: 
                  (a)to add a student
                  (r)to remove a student 
                  (m)to modify a grade
                  (p)print all grades
                  (q)to quit
                  Enter choice:  """);
            String strTemp = in.nextLine();
            //store user choice
            cUserInput = strTemp.charAt(0);
            //store user's name and grade
            String strFirst = "";
            String strGrade = "";
            switch(cUserInput){
                case 'a':
                    //asks user to enter info
                    strFirst = getName(1);
                    strGrade = getGrade();
                    //if both aren't blank
                    if(!strFirst.equals("") && !strGrade.equals("")){
                        studentGrades.put(strFirst,strGrade);
                        System.out.println("Added student successfully");
                    }
                    break;
                case 'r':
                    //get user name
                    strFirst = getName(1);
                    //check if student in hashmap
                    if(studentGrades.containsKey(strFirst)){
                        studentGrades.remove(strFirst);
                        System.out.println("Student removed successfully");
                    }// if student isn't in hashmap
                    else {
                        System.out.println("Student doesn't exist");
                    }
                    break;
                case 'm':
                    strFirst = getName(1);
                    //if student exists
                    if(studentGrades.containsKey(strFirst)){
                        strGrade = getGrade();
                        //update new grade
                        studentGrades.put(strFirst,strGrade);
                        System.out.println("Student grade modified successfully");
                    }
                    else {//if student doesn't exist
                        System.out.println("Student doesn't exist");
                    }
                    break;
                case 'p':
                    //print all of hashmap
                    if(!studentGrades.isEmpty()) {
                        for (Map.Entry<String, String> temp : studentGrades.entrySet()) {
                            //storing keys and values
                            String strkey = temp.getKey();
                            String strVal = temp.getValue();
                            System.out.println(String.format("%s: %s", strkey, strVal));
                        }
                    }
                    else {//if no students in hashmap
                        System.out.println("No students");
                    }
                    break;
                case 'q':
                    break;
                default:
                    System.out.println(String.format("%s is not a correct input",strTemp));
                    System.out.println();
            }
        }

    }
}
