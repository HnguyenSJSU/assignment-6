package com.company;
/**
 * Hung Nguyen
 * 013626210
 * Class Student that implements with comperable
 */

import java.util.Objects;

/**
 * class student that implements with comparable interface
 */
public class Student implements Comparable<Student> {
    //Fields to store student info
    private String strFname;
    private String strLname;
    private String strID;

    /**
     * Constructor of student
     * @param strFtemp
     * @param strLtemp
     * @param strIDtemp
     */
    public Student(String strFtemp, String strLtemp, String strIDtemp){
        strFname = strFtemp;
        strLname = strLtemp;
        strID = strIDtemp;
    }

    /**
     * compare student objects with each other
     * @param student
     * @return 0 or 1
     */
    @Override
    public int compareTo(Student student) {
        int nFname = this.strFname.compareTo(student.strFname);
        int nLast = this.strLname.compareTo(student.strLname);
        //if nLast isn't the same
        if(nLast != 0) {
            return nLast;
        }
        //if nFname isn't the same
        else if (nFname != 0){
            return nFname;
        }
        return this.strID.compareTo(student.strID);
    }

    /**
     * override hashcode with variables
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(strFname,strLname,strID);
    }

    /**
     * getters for first name
     * @return strFname
     */
    public String getStrFname(){
        return strFname;
    }

    /**
     * getters for last name
     * @return strLname
     */
    public String getStrLname(){
        return strLname;
    }

    /**
     * getters for ID
     * @return strID
     */
    public String getStrID(){
        return this.strID;
    }

    /**
     * print instances to string
     * @return string
     */
    public String toString(){
        return String.format("%s %s (ID=%s): ", this.strFname,this.strLname,this.strID);
    }

    /**
     * setters for firstname
     * @param strTemp
     */
    public void setStrFname(String strTemp){
        this.strFname = strTemp;
    }

    /**
     * setters for last name
     * @param strTemp
     */
    public void setStrLname(String strTemp){
        this.strLname = strTemp;
    }

    /**
     * setters for id
     * @param strTemp
     */
    public void setStrID(String strTemp){
        this.strID = strTemp;
    }
}
