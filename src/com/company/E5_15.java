package com.company;
/**
 * Hung Nguyen
 * 013626210
 * Find unique identifiers and print which line it occurs on.
 */

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Print Identifiers occurence
 */
public class E5_15 {

    public static void main(String[] args) throws IOException {

        try {
            String strFileName = args[0];
            String strFilePath = "D:\\cs49j\\Assignment6\\src\\com\\company\\" + strFileName;
            //Reads file path
            FileReader appFile = new FileReader(strFilePath);
            //store file lines
            HashMap<Integer, String> fileLines = new HashMap<>();
            //store identifers in each line
            LinkedHashMap<String, Integer> IdentifierList = new LinkedHashMap<String, Integer>();
            //read line by line with getLine method

            LineNumberReader inputFile = new LineNumberReader(appFile);

            //while file not empty
            while (inputFile.ready()) {

                String strLine = inputFile.readLine();

                int nLineNumber = inputFile.getLineNumber();
                //if hashmap already has the line, skip it
                if(fileLines.containsKey(strLine) == false) {
                    if(!strLine.equals("")) {
                        fileLines.put(nLineNumber, strLine);
                    }
                }
                Scanner in = new Scanner(strLine).useDelimiter("[^A-Za-z0-9_]+");
                while (in.hasNext()) {
                    String strTemp = in.next();
                    //if identifer isn't unique
                    if(IdentifierList.containsKey(strTemp) || strTemp.equals("company" )
                    || strTemp.equals("com" )|| strTemp.equals("package" )){
                        continue;
                    }
                    else{
                        IdentifierList.put(strTemp,nLineNumber);
                    }
                }
            }
            int nCount = 0;
            //runs through Linked Hashmap and print existing keys
            for(Map.Entry<String,Integer> temp: IdentifierList.entrySet()){

                System.out.println(String.format("%s: %s occurs in: ",nCount,temp.getKey()));
                if(fileLines.containsKey(temp.getValue())){
                    System.out.println(fileLines.get(temp.getValue()));
                }
                nCount++;

            }

        }
        catch (IOException e){
            System.out.println("file not found");

        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Need a command line argument for the file to read.");
        }
    }
}


